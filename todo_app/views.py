from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import Todo


def index(request):
    if request.method == 'POST':
        text = request.POST['text']
        todo = Todo()
        todo.user = request.user
        todo.text = text
        todo.save()

        todos = Todo.objects.filter(user=request.user, status=False)
        context = {
            'todos': todos
        }
    return render(request, 'todo_app/index.html', context)


def change_status(request):
    pk = request.POST['pk']
    todo = get_object_or_404(Todo, pk=pk)
    if 'checked' in request.POST:
        todo.status = True
    else:
        todo.status = False
    todo.save()
    if 'completed_todos' in request.META['HTTP_REFERER']:
        return HttpResponseRedirect(reverse('todo_app:completed_todos'))
    else:
        return HttpResponseRedirect(reverse('todo_app:index'))


def completed_todos(request):
    todos = Todo.objects.filter(status=True)
    context = {
        'todos': todos
    }
    return render(request, 'todo_app/completed_todos.html', context)


def delete_todo(request):
    pk = request.POST['pk']
    todo = get_object_or_404(Todo, pk=pk)
    todo.delete()
    if 'completed_todos' in request.META['HTTP_REFERER']:
        return HttpResponseRedirect(reverse('todo_app:completed_todos'))
    else:
        return HttpResponseRedirect(reverse('todo_app:index'))
